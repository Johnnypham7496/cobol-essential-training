       IDENTIFICATION DIVISION.
       PROGRAM-ID. EXAMPLE.
       AUTHOR. JOHNNY PHAM.
       
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 NAME PIC X(20).
       01 AGE PIC 99.
       
       PROCEDURE DIVISION.
       DISPLAY "What is your name? ".
       ACCEPT NAME.
       
       DISPLAY "What is your age? ".
       ACCEPT AGE.
       
       DISPLAY "Hello, " NAME ", you are " AGE " years old.".
       
       STOP RUN.
       