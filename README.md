# COBOL Essential Training


my example COBOL project

Currently following a course through LinkedIn Learning for COBOL Essential Training. Click [here](https://www.youtube.com/watch?v=_pAX_ogguLI&list=PLCavSfR5FZZ_SjN8HT4Ib1saoc_k-p7F4&index=66&t=1028s&ab_channel=missgoogle) to follow along the Youtube video instead

Click [here](https://mega.nz/file/52JjTSpK#CqwBDZPZCyQngt5zWFoYgJoJWhiDCYoj0IDzaa7hclU) to access COBOL tutorial course exercise files to follow along the course.

COBOL tutorial: https://www.tutorialspoint.com/cobol/index.htm
<hr>

## What is COBOL?

- COBOL stands for Common Business Oriented Language. It is imperative, procedural, and object-oriented. A compiler is a computer program that takes other computer programs written in a high-level (source) language and coverts them into another program, machine code, which the computer can understand. COBOL takes data from a file or database, processes, and outputs it. In short: COBOL takes data in, computes it, and outputs it afterwards

- COBOL programs are divided into four divisions: Identification, Environment, Data, and Procedure. The Identification division contains the program name, author name, and other identifying information. The Environment division contains system-specific information such as file names and printer types. The Data division describes the data used in the program, including the names of variables, their data types, and their initial values. Finally, the Procedure division contains the actual program code.

- Despite being an older language, COBOL is still used in many legacy systems and mainframes. In recent years, there has been renewed interest in COBOL due to the COVID-19 pandemic and the need to process large amounts of data in government systems.

- COBOL is primarily used in business, finance, and administrative systems for companies and governments. COBOL is still widely used in applications deployed on mainframe computers, such as large-scale batch and transaction processing jobs.
<hr>


## Getting Started

- We will be utilizing Emulators to allow COBOL to be compiled and run on our personal computers

- Installation steps and documentation:
    - [WSL/Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/install)
    - [Debian](https://apps.microsoft.com/store/detail/debian/9MSVKQC78PK6?hl=en-us&gl=us&rtc=1)
    - [GnuCOBOL](https://sourceforge.net/projects/gnuCOBOL/)
    - [VS Code](https://code.visualstudio.com/docs/remote/wsl)

<hr>

## Structure

PROGRAM --> DIVISION --> SECTIONS --> PARAGRAPHS --> SENTENCES --> STATEMENTS --> CHARACTERS

- COBOL must be written in a format that is acceptable to the compilers

- 80 characters are present in coding area

- Character positions are grouped into: 
    - Column numbers(1-6): Reserved for line numbers
    - Indicator(7): It can have Asterisk(*) indicatin comments, hyphen(-), indication continuation, and slash(/) indicating form feed
    - Area A(8-11): All COBOL divisions, sections, paragraphs and some special entries must begin in Area A
    - Area B(12-72): All COBOL statements must begin in Area B
    - Identification Area(73-80): It can be used as needed by the programmer(normally not used)

<hr>

## Division

### COBOL consists of 4 division to make up the code

    - Identification: Name of the program ID, author name(optional)

    - Environment: Contains configuration section the provides information about the system to which the program is written and executed(optional). Another section is the INPUT-OUPUT section which provides information about the files to be used in the program. 

    - Data: Used to define varibles used within the program which will have 4 sections
        File: Defines the structure of the file

        Working Storage: Declare temporary variables and file structure used within the program

        Local storage: Similar to working storage but have saved varibles that will run everytime the program is executed

        Linkage: Describe the dataset names recieved from an external program

    - Procedure: Used to include the logic of the program as known as functions

<hr>

## Data Types

### Syntax: Data Division is used to define the variabless used in a program.
    Level-number --> Data-name --> Picture-clause --> Value-clause

    - Level number is used to specify the level of the data in a record. They are used to differentiate between elementary items and group items.
        01: Record description entry
        02 to 49: Group and Elementary items
        66: rename clause items
        77: Items which cannot be sub-divided
        88: Condition name entry

    - Data Name: Data names must be defined in the Data Division before using in the Procedure Division, Must have a have a user-defined name, Reserved words cannot be used.

    - Picture clause: Used to define the data type.
        9: Numberic
        A: Alphabetic
        X: Alphanumeric 
        V: Implicit Decimal
        S: Sign
        P: Assemed Decimal

    ### Value Clause: Value clause is an optional clause which is used to initialize the data items.

<hr>

## Basic Verbs

### Accept: Accept verb is used to get data such as date, time, and day from the operating system or directly the user.
    Syntax: ACCEPT DATE/data-name
    
### Display: Display verb is used to display the output of a COBOL program.
    Syntax: DISPLAY data-name/message
### Initialize: Initialize verb is used to initialize a group item or an elementary item.
    Syntax: INITIALIZE data-name

### Move: Move verb is used to capy data from source data to destination data.
    Syntax: MOVE data-name1 TO data-name2

### Add: Add verb is used t add two or more numbers and store the results in the destination operand.
    Syntax: ADD A TO C GIVING B
### Subtract: Subtract verb is usedd for subtration operations.
    Syntax: SUBTRACT A FROM B GIVING C
### Multiply: Multiply verb is used for multiplication operations.
    Syntax: MULTIPLY A BY B GIVING E
### Divide: Divide verb is used for division operations. 
    Syntax: DIVIDE A BY B GIVING C REMAINDER R
### Compute: Compute statement us used to write arithmetic expressions in COBOL, This is a replacement for Add, Subtract, Multiply, Divide.
    Syntax: COMPUTE A = (B * C) + WS-NUM3

<hr>

## Data Layout

### Redefines Clause
    - Used to define a storage with different data description.
    - If one or more data items are not used simutaneously, then the same storage can be utilized for another data item.
    - Level numbers of redefined item and redefing item must be the same and cannot be 66 or 88 level number.
    - Do not use VALUE clause with a redefing item.
    - In File Section, do not use a redefines clause with 01 level number. Redefines definition must be the next data description ou want to redefine.
    - A redefing item will always have the same value as a redefined item.

    - Syntax:
        01 WS-OLD PIC X(10).
        01 WS-NEW REDEFINES WS-OLD PIC 9(8).

        WS-OLD is Redefined item
        WS-NEW is Redefining item 
<br>

### Rename Clause 
    - Used to give different names to existing data items. 
    - It is used to re-group the data names and give a new name to them. 
    - The new data names can rename across groups or elementary items. Level number 66 is reserved for renames.
    - Renaming is possible at the same level only.
    - Renames definition must be the next data description you want to rename. 
    - Do not use Renames with 01, 77, or 66 level numbers.
    - The data names used for renames must come in sequence. Data items with occur clause cannot be renamed.

    Syntax:
        01 WS-OLD
            10 WS-A PIC 9(12).
            10 WS-B PIC X(20).
            10 WS-D PIC X(12).
                66 WS-NEW RENAMES WS-A THRU WS-B

<br>

### Usage Clause
    - Usage clause specifies the operating system in which the format data is stored.
    - It cannot be used with level numbers 66 or 88.
    - If usage clause is specified on a group, then all the elementary items will have the same usage clause.

    - Display
        Data item is stored in ASCII format and each cahracter will take 1 byte. It is default usage.
    - Computational/COMP
        Data item is stored in binary format. Here data items must be integer

    COMP-1
    Data item is similar to Real or Float and is represented as a single precision floating point number. Internally data is stored in hexadecimal format. COMP-1 does not accept PIC clause. Here 1 word is equal to 4 bytes. 

    COMP-2 
    Data item is similar to Long or Double and is represented as double precision floating point number. Interally data is stored in hexadecimal format. COMP-2 does not specify PIC clause. Here 2 words is equal to 8 bytes.

    COMP-3
    Data item is stored in pack decimal format. Each digit occupies half a byte (1 nibble) and the sign is stored at the right most nibble. 

<br>

### Copybooks
    - A COBOL copybook is a selection of code that defines data structures. 
    - If a particular data structure is used in many programs then instead of writing the same data structure again, we can use copybooks.
    - COPY statement is used in the Working-Storage Section.

    Syntax: 
        WORKING-STORAGE SECTION
            COPY ABC

<hr>

## Conditional Statements

### IF-ELSE
    - If statements checks for conditions.
    - If a condition is true the IF clock is executed and if the condition is false, the ELSE black is executed.
    - END-IF is used to the end the IF block. To end the IF block, a period can be used instead of END-IF.
    
    Syntax: 
        IF [condition] THEN
        [COBOL statements]
        ELSE
            [COBOL statements]
        END-IF.

### Sign Condition
    - Sign condition is used to check the sign of a numeric operand. 
    - It determines whether a given numberic value is greater than, less than, or equal to ZERO.

    Syntax: 
        [Data Name/Arithmetic Operation]
            [IS][NOT]
        [Positive, Negative or Zero]
### Class Condition
    - Class condition is used to check if an operand contains only alphabets or numeric data.
    - Checks for uppcase, lowercase, or alphabetic. Spaces are also considered
### 88 Level Condition
    - Condition name is user defined name.
    - It contains a set of value specified by the user.
    - They are defined with the level number 88.
    - It will not have a PIC clause. 

    Syntax:
        88[Condition-Name]VALUE[IS,ARE][LITERAL].
### Negated Condition
    - Negated condition is given by using the NOT keyword.
    - If a condition is true and we have given NOT in front of it, then its final value will be false.
### Evaluate Condition
    - Evaluate verb is a replacement of series of IF-ELSE statement. 
    - It can be used to evaluate more than one condition.
    - It is similar to SWITCH statement in C programs.

    Syntax:
        EVALUATE TRUE
            WHEN 
                DISPLAY 'WS-A GREATER THAN 2'
            WHEN OTHER 
                DISPLAY 'INVALID VALUE OF WS-A'
        END-EVALUATE.

<hr>

## Loop Statements

### Preform Thru
    - Preform Thru is used to exeute a series of paragraph by giving the first and last paragraph names in the sequence. 
    - After executing the last paragraph control is returned back.

    Syntax:
        PREFORM PARAGRAPH1 THRU PARAGRAPH3

### Preform Until
    - In preform until, a paragraph is executed until the given condition becomes true.
    - With test before is the default condition and it indicates that the conditino is checked before the execution of statements in a paragraph

    Syntax:
        PREFORM A-PARA UNTIL condition

### Preform Times
    - In 'preform times', a paragraph will be executed the number of times specified.

    Syntax:
        PREFORM A-PARA 5 TIMES

### Preform Varying
    - In preform varying, a paragrap wll be executed till the condition in Until phrase becomes true

    Syntax:
        PREFORM A-PARA VARYING A FROM 1 BY 1 UNTIL A<5.

### Go To
    - GO TO statements is used to change the flow of execution in a program.
    - In GO TO statements transfer goes only in the forward direction. 
    - It is used to exit a paragraph.

    Syntax:
        Unconditional GO TO
            GO TO para-name

        Conditional GO TO
            GO TO para-1 DEPENDING ON x.